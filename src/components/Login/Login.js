import React, { Component } from "react";
import {NavLink } from 'react-router-dom';
import './Login.css'; 

const navStyles={
  display: 'flex',
  justifyContent: 'space-around'
}


const MenuPrincipal = (props) => (  
  <div class="wrapper fadeInDown">
  <div id="formContent">
  <br></br>         
  <center> <h1>Menu Principal</h1> </center>
  <nav style={navStyles}>
    <NavLink to={{
      pathname : '/crearCliente',
      state: {
        token: props.token
      }
    }}> Crear Cliente</NavLink>
    <NavLink to={{
      pathname : '/crearUsuarios',
      state: {
        token: props.token
      }
    }}> Crear Usuario</NavLink>   
    <NavLink to={{
      pathname : '/',
      state: {
        token: props.token
      }
    }}> Salir</NavLink>     
    </nav>
    </div>
</div>  
)

 class Login extends Component {
  constructor(props) {
    super(props);
    this.hacerlogin = this.hacerlogin.bind(this);
   

    this.state = {
      login:'',
      password: '',
      mensajeError: '' ,
      published: false,
      submitted: false,
      token: '',
      pintarLogin: true
    };
  }
	
	eventoCambioValorCampo = (event) => {
        const value = event.target.value
        const name = event.target.name
        this.setState({[name]: value});
    }

    componentDidMount(props){
      if(this.state.pintarLogin){
         document.getElementById("mensajeError").style.display = "none"; 
        }
    }

    handleError = (error) => {
        console.log(error.message);
        this.setState({mensajeError: 'Error al consumir el servicio de login'});
        document.getElementById("mensajeError").style.display = "";
    }

	hacerlogin  () {  		
        
        fetch (`https://dvbw90z1y5.execute-api.us-east-1.amazonaws.com/maestria/login?user_name=${this.state.login}&password=${this.state.password}`, {
        method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json', // <-- Specifying the Content-Type
            }),     
        })
        .then(res => res.json())
        .then((data) => {     
         
         console.log(data.code)
        if(data.code === '200'){                        
            sessionStorage.setItem('token', data.data)
                        
            this.setState({
                submitted: true,
                token: data.data
                });    
            
         }else{
            console.log(" no pase")      
            document.getElementById("mensajeError").style.display = "";
            this.setState({mensajeError: data.message});
         }
        })
        .catch(error => {
          this.handleError(error);
        });
        
    } 


  render(){
        return (
		 <div className="submit-form">
		 {
             this.state.submitted ? (
          <div>
            <MenuPrincipal />
          </div>
        ) :(          
            <div class="wrapper fadeInDown">
                <div id="formContent">
                    <form >
                        <br></br>
                        <div class="fadeIn first">
                            <img src="login.png" id="icon" alt="User Icon" />
                        </div>
                            <div class="alert alert-danger" id="mensajeError" >{this.state.mensajeError}
                        </div>
                       
                        <input type="text" value={this.state.login}     onChange={this.eventoCambioValorCampo} id="login" class="fadeIn second" name="login" placeholder="Usuario" />
                        <input type="text" value={this.state.password}  onChange={this.eventoCambioValorCampo} id="password" class="fadeIn third" name="password" placeholder="Contraseña" />
                        <input type="button"  class="fadeIn fourth"  value="Ingresar" onClick={this.hacerlogin}/>

                        <div id="formFooter">
                           <a class="underlineHover" href="/cambioPassword" >Cambiar contraseña</a>
                        </div>
          
                    </form>   
                </div>    
            </div>         
			)
		 }			
		</div>	
        )
    }

   
}
const Login1  = () => (                   
  <Login/>                      
)
export default Login1;