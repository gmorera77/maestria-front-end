import React from 'react';
import {NavLink } from 'react-router-dom';

const navStyles={
  display: 'flex',
  justifyContent: 'space-around'
}

const MenuPrincipal = () => (
  <div class="wrapper fadeInDown">
  <div id="formContent">
  <br></br>         
  <center> <h1>Menu Principal</h1> </center>
  <nav style={navStyles}>
    <NavLink to={{
      pathname : '/crearCliente',
      state: {
        token: 'dasdasd'
      }
    }}> Crear Cliente</NavLink>
    <NavLink to={{
      pathname : '/crearUsuarios',
      state: {
        token: 'dasdasd'
      }
    }}> Crear Usuario</NavLink>   
    <NavLink to={{
      pathname : '/',
      state: {
        token: 'dasdasd'
      }
    }}> Salir</NavLink>     
    </nav>
    </div>
</div>  
)


export default MenuPrincipal;


