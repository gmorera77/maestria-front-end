import React,{Component} from 'react';
import './CrearUsuarios.css'; 
import {NavLink } from 'react-router-dom';

const navStyles={
    display: 'flex',
    justifyContent: 'space-around'
  }

class CrearUsuarios1 extends Component {

    state = {
        usuario:'',
        email:'',
        nombre: '',
        celular: '',     
        mensajeError: '',
        estiloError: false
    }  

  componentDidMount(){
    document.getElementById("mensajeError").style.display = "none";
  }

  eventoCambioValorCampo = (event) => {
    const value = event.target.value
    const name = event.target.name
    this.setState({[name]: value});
}
  
     
    crearUsuarioAction = () => {  
    this.setState({mensajeError: ''}); 
    document.getElementById("mensajeError").style.display = "none";
    fetch (`https://dvbw90z1y5.execute-api.us-east-1.amazonaws.com/maestria/usuarios?usuario=${this.state.usuario}&email=${this.state.email}&nombre=${this.state.nombre}&celular=${this.state.celular}`, {            
      method: 'POST',      
      headers: new Headers({
        'Content-Type': 'application/json', 
        'Authorization': sessionStorage.getItem('token')
      }),     
    })
    .then(res => res.json())
    .then((data) => {     
     
     console.log(data.code)
     if(data.code === '200'){
      console.log("se creo el usuario")
      document.getElementById("mensajeError").style.display = "";
      this.setState({usuario:'',email:'',nombre: '',celular: '',mensajeError: 'Se creo el usuario exitosamente' , estiloError: false});

     }else{
      console.log(" no se creo el usuario")      
      document.getElementById("mensajeError").style.display = "";
      this.setState({mensajeError: data.message , estiloError: true});
     }
    })
    .catch(error => {
      this.handleError(error);
    });
}

handleResponseError(response) {
  throw new Error("HTTP error, status = " + response.status);
}


handleError = (error) => {
  console.log(error.message);
  this.setState({mensajeError: 'Error al consumir el servicio de crear usuario', estiloError: true});
  document.getElementById("mensajeError").style.display = "";
}

  
 
  render() {      
    const { estiloError } = this.state;
      return (      
      <div class="wrapper fadeInDown">
        <div id="formContent">
        <br></br>
        <div class="fadeIn first">
          
                 
        </div>
          <div class={`box ${estiloError ? "alert alert-danger" : "alert alert-success"}`} id="mensajeError" >{this.state.mensajeError}
        </div>
        <center> <h1>Crear Usuario</h1> </center>
        <form >

        <nav style={navStyles}>
        <NavLink to={{
            pathname : '/menuPrincipal',
            state: {
            token: 'dasdasd'
          }
          }}> Home</NavLink>
        </nav>
        
        
        <center><table class="egt">
        <tr>
        <td>Usuario</td>
        <td><input type="text" value={this.state.usuario} onChange={this.eventoCambioValorCampo} id="usuario" class="fadeIn second" name="usuario"  /></td>        
        </tr>

        <tr>
        <td>Email</td>
        <td><input type="text" value={this.state.email} onChange={this.eventoCambioValorCampo} id="email" class="fadeIn second" name="email"  /></td>        
        </tr>

        <tr>
        <td>Nombre</td>
        <td><input type="text" value={this.state.nombre} onChange={this.eventoCambioValorCampo} id="nombre" class="fadeIn second" name="nombre"  /></td>        
        </tr>

        <tr>
        <td>Celular</td>
        <td><input type="text" value={this.state.celular} onChange={this.eventoCambioValorCampo} id="celular" class="fadeIn second" name="celular"  /></td>        
        </tr>
       

        </table></center>
      
        <input type="button"  class="fadeIn fourth"  value="Crear" onClick={this.crearUsuarioAction}/>
               
        </form>                            
      </div>
    </div>     
        );
  };   
  
}

const CrearUsuarios = () => (                   
    <CrearUsuarios1/>                      
)

export default CrearUsuarios;