import React, { Component } from 'react';
import './CrearCliente.css'; 
import {NavLink } from 'react-router-dom';

const navStyles={
    display: 'flex',
    justifyContent: 'space-around'
  }

class CrearCliente1 extends Component {

    state = {
        tipoDocumento:'',
        numeroDocumento:'',
        nombre: '',
        primerApellido: '',
        segundoApellido: '',
        mensajeError2: '',
        estiloError: true        
    }  

    componentDidMount(){
        document.getElementById("mensajeError2").style.display = "none";
      }
    
      
      crearClienteAction = (props) => {  
        var formData = `{ "tipoDocumento":"${this.state.tipoDocumento}","numeroDocumento":"${this.state.numeroDocumento}","nombre":"${this.state.nombre}","primerApellido":"${this.state.primerApellido}","segundoApellido":"${this.state.segundoApellido}" }`;
        this.setState({mensajeError2: ''}); 
        document.getElementById("mensajeError2").style.display = "none";
        fetch (`https://dvbw90z1y5.execute-api.us-east-1.amazonaws.com/maestria/clientes`, {
          method: 'POST',
          body: formData,
          headers: new Headers({
            'Content-Type': 'application/json', 
            'Authorization': sessionStorage.getItem('token')
          }),     
        })
        .then(res => res.json())
        .then((data) => {     
         
         console.log(data.code)
         if(data.code === '200'){
          document.getElementById("mensajeError2").style.display = "";
          this.setState({mensajeError2: 'Se creo exitosamente el cliente', tipoDocumento:'',
          numeroDocumento:'',
          nombre: '',
          primerApellido: '',
          segundoApellido: '',
          estiloError: false });

          console.log("se creo el cliente")
          
         }else{
          console.log(" no se creo el cliente")      
          document.getElementById("mensajeError2").style.display = "";          
          this.setState({mensajeError2: data.message,stiloError: true });
         }
        })
        .catch(error => {
          this.handleError(error);
        });
    }
    
    handleResponseError(response) {
      throw new Error("HTTP error, status = " + response.status);
    }
    
    
    handleError = (error) => {
      console.log(error.message);      
      this.setState({mensajeError2: 'Error al consumir el servicio de crear cliente' , stiloError: true});
      document.getElementById("mensajeError2").style.display = "";
    }
    
    
    eventoCambioValorCampo = (event) => {
      const value = event.target.value
      const name = event.target.name
      this.setState({[name]: value});
  }

      render() {      
        const { estiloError } = this.state;
        return (      
        <div class="wrapper fadeInDown">
          <div id="formContent">
          <br></br>
          <div class="fadeIn first">
          </div>          
            <div class={`box ${estiloError ? "alert alert-danger" : "alert alert-success"}`} id="mensajeError2" >{this.state.mensajeError2}
          </div>
          <center> <h1>Crear Clientes</h1> </center>        
          <form >
  
          <nav style={navStyles}>
          <NavLink to={{
              pathname : '/menuPrincipal',
              state: {
              token: 'dasdasd'
            }
            }}> Home</NavLink>
          </nav>
                 
          <center><table class="egt">
          <tr>
          <td>Tipo Documento</td>
          <td><input type="text" value={this.state.tipoDocumento} onChange={this.eventoCambioValorCampo} id="tipoDocumento" class="fadeIn second" name="tipoDocumento"  /></td>        
          </tr>
  
          <tr>
          <td>Numero documento</td>
          <td><input type="text" value={this.state.numeroDocumento} onChange={this.eventoCambioValorCampo} id="numeroDocumento" class="fadeIn second" name="numeroDocumento"  /></td>        
          </tr>
  
          <tr>
          <td>Nombre</td>
          <td><input type="text" value={this.state.nombre} onChange={this.eventoCambioValorCampo} id="nombre" class="fadeIn second" name="nombre"  /></td>        
          </tr>
  
          <tr>
          <td>Primer apellido</td>
          <td><input type="text" value={this.state.primerApellido} onChange={this.eventoCambioValorCampo} id="primerApellido" class="fadeIn second" name="primerApellido"  /></td>        
          </tr>
  
          <tr>
          <td>Segundo apellido</td>
          <td><input type="text" value={this.state.segundoApellido} onChange={this.eventoCambioValorCampo} id="segundoApellido" class="fadeIn second" name="segundoApellido"  /></td>        
          </tr>
  
          </table></center>      
          <input type="button"  class="fadeIn fourth"  value="Crear" onClick={this.crearClienteAction}/>
  
          
                 
          </form>                            
        </div>
      </div>     
          );
    }; 

      
}

const crearCliente = () => (                   
    <CrearCliente1/>                      
)

export default crearCliente;