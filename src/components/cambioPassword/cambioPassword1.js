import React,{Component} from 'react';
import './CambioPassword.css'; 


class CambioPassword1 extends Component {

    state = {
      login:'',
      passwordTemporal:'',
      passwordNuevo: '',
      verificacionPasswordNuevo: '',
      mensajeError: ''      
    } 
    
  componentDidMount(){
    document.getElementById("mensajeError").style.display = "none";
  }

  eventoCambioValorCampo = (event) => {
    const value = event.target.value
    const name = event.target.name
    this.setState({[name]: value});
}

  
    
    cambioPasswordAction = () => {
    this.setState({mensajeError: ''}); 
    document.getElementById("mensajeError").style.display = "none";
    fetch (`https://dvbw90z1y5.execute-api.us-east-1.amazonaws.com/maestria/login/cambiopassword?user_name=${this.state.login}&old_password=${this.state.passwordTemporal}&new_password=${this.state.passwordNuevo}&verify_password=${this.state.verificacionPasswordNuevo}`, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json', // <-- Specifying the Content-Type        
      }),     
    })
    .then(res => res.json())
    .then((data) => {     
     
     console.log(data.code)
     if(data.code === '200'){
      console.log("pase")

      document.getElementById("mensajeError").style.display = "";
      this.setState({mensajeError: 'Usuario actualizado exitosamente' ,estiloError: false ,   login:'',
      passwordTemporal:'',
      passwordNuevo: '',
      verificacionPasswordNuevo: ''});

     }else{
      console.log(" no pase")      
      document.getElementById("mensajeError").style.display = "";
      this.setState({mensajeError: data.message , estiloError: true});
     }
    })
    .catch(error => {
      this.handleError(error);
    });
}

handleResponseError(response) {
  throw new Error("HTTP error, status = " + response.status);
}


handleError = (error) => {
  console.log(error.message);
  this.setState({mensajeError: 'Error al consumir el servicio de cambio contraseña' ,estiloError: true});
  document.getElementById("mensajeError").style.display = "";
}

 
  render() {      
    const { estiloError } = this.state;
      return (      
      <div class="wrapper fadeInDown">
        <div id="formContent">
        <br></br>
        <div class="fadeIn first">
          <img src="login.png" id="icon" alt="User Icon" />
                 
        </div>
          <div class={`box ${estiloError ? "alert alert-danger" : "alert alert-success"}`} id="mensajeError" >{this.state.mensajeError}
        </div>       
        
        <form >       

        <input type="text" value={this.state.login}     onChange={this.eventoCambioValorCampo} id="login" class="fadeIn second" name="login" placeholder="Login" />
          <input type="text" value={this.state.passwordTemporal}     onChange={this.eventoCambioValorCampo} id="passwordTemporal" class="fadeIn second" name="passwordTemporal" placeholder="Contraseña actual" />
          <input type="text" value={this.state.passwordNuevo}  onChange={this.eventoCambioValorCampo} id="passwordNuevo" class="fadeIn third" name="passwordNuevo" placeholder="Nueva contraseña" />
          <input type="text" value={this.state.verificacionPasswordNuevo}  onChange={this.eventoCambioValorCampo} id="verificacionPasswordNuevo" class="fadeIn third" name="verificacionPasswordNuevo" placeholder="Verificación nueva contraseña" />
          <input type="button"  class="fadeIn fourth"  value="Cambiar" onClick={this.cambioPasswordAction}/>

          <div id="formFooter">
            <a class="underlineHover" href="/" >Regresar</a>
          </div>
                
        </form>                            
      </div>
    </div>     
        );
  };   
}

const CambioPassword = () => (                   
    <CambioPassword1/>                      
)

export default CambioPassword;