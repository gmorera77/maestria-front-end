import React from 'react';
import './App.css';

import CambioPassword from './components/cambioPassword/cambioPassword1';
import MenuPrincipal from './components/menuPrincipal/MenuPrincipal';
import CrearCliente from './components/crearCliente/crearCliente1';
import CrearUsuarios from './components/crearUsuarios/CrearUsuarios1';
import {BrowserRouter , Route} from 'react-router-dom';
import Login from './components/Login/Login';


const App = () => {
return(
  <BrowserRouter>  
    <Route path='/' exact render={Login}/>   
    <Route path='/menuPrincipal' render={MenuPrincipal}/> 
    <Route path='/crearCliente' render={CrearCliente}/>  
    <Route path='/crearUsuarios' render={CrearUsuarios}/> 
    <Route path='/cambioPassword' render={CambioPassword}/> 
  </BrowserRouter>
)
}

export default App;
